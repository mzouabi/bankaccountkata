package com.exteam.mzouabi.kata.server.dto;

import de.malkusch.validation.constraints.banking.BIC;
import de.malkusch.validation.constraints.banking.IBAN;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by mouradzouabi on 04/12/15.
 */
public class AccountDTO extends AbstractDTO {

    @NotNull
    @IBAN
    String iban;

    @NotNull
    @BIC
    String bic;

    BigDecimal balance;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}
