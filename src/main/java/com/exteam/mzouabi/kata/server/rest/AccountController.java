package com.exteam.mzouabi.kata.server.rest;

import com.exteam.mzouabi.kata.server.dto.AccountDTO;
import com.exteam.mzouabi.kata.server.dto.TransactionDTO;
import com.exteam.mzouabi.kata.server.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountController {

    final static Logger LOG = LoggerFactory.getLogger(AccountController.class);

    @Inject
    AccountService accountService;

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Long createAccount(@RequestBody @Validated AccountDTO accountDTO) {
        return accountService.createAccount(accountDTO);
    }

    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDTO> getAccount(@PathVariable Long accountId, HttpServletRequest req) {
        AccountDTO account = accountService.getAccount(accountId);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/{accountId}/deposit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deposit(@PathVariable Long accountId, @RequestBody BigDecimal amount) {
        accountService.deposit(accountId, amount);
    }

    @RequestMapping(value = "/{accountId}/withdraw", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public void widthdraw(@PathVariable Long accountId, @RequestBody BigDecimal amount) {
        accountService.withdraw(accountId, amount);
    }

    @RequestMapping(value = "/{accountId}/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<TransactionDTO>> getTransactions(@PathVariable Long accountId, Pageable pageable) {
        Page<TransactionDTO> transactionsPage = accountService.getTransactions(accountId, pageable);
        return new ResponseEntity<>(transactionsPage, HttpStatus.OK);
    }
}


