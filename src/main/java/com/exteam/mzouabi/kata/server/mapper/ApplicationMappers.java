package com.exteam.mzouabi.kata.server.mapper;

import com.exteam.mzouabi.kata.server.dto.AccountDTO;
import com.exteam.mzouabi.kata.server.dto.TransactionDTO;
import com.exteam.mzouabi.kata.server.model.Account;
import com.exteam.mzouabi.kata.server.model.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

/**
 * Created by mouradzouabi on 04/12/15.
 */
@Mapper(componentModel = "spring" , unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ApplicationMappers {

    AccountDTO toAccountDTO(Account person);

    Account toAccountEntity(AccountDTO person);

    void mapToAccountEntity(AccountDTO personDTO, @MappingTarget Account person);

    TransactionDTO toTransactioDTO(Transaction transaction);

    Transaction toTransactionEntity(TransactionDTO transactionDTO);

    void mapToTransactionEntity(TransactionDTO transactionDTO, @MappingTarget Transaction transaction);

}
