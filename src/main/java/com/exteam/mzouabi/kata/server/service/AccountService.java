package com.exteam.mzouabi.kata.server.service;

import com.exteam.mzouabi.kata.server.dto.AccountDTO;
import com.exteam.mzouabi.kata.server.dto.TransactionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * Méthode qui crée un compte
     * @param accountDTO
     * @return
     */
    Long createAccount(AccountDTO accountDTO);

    /**
     * Méthode qui renvoie un compte
     *
     * @param accountId
     * @return
     */
    AccountDTO getAccount(Long accountId);

    /**
     * Méthode qui réalise un dépôt
     *
     * @param accoutnId
     * @param amount
     */
    void deposit(Long accoutnId, BigDecimal amount);

    /**
     * Méthode qui réalise un retrait
     *
     * @param accoutnId
     * @param amount
     */
    void withdraw(Long accoutnId, BigDecimal amount);

    /**
     * Méthode qui renvoie la liste des transacions réalisées sur un compte ( une page)
     *
     * @param accoutnId
     * @param pageable
     * @return
     */
    Page<TransactionDTO> getTransactions(Long accoutnId, Pageable pageable);

}
