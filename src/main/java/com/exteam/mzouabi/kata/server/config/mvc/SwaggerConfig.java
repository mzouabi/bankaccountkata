package com.exteam.mzouabi.kata.server.config.mvc;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("webservice-api").select()
                .paths(paths())
                .build().apiInfo(apiInfo());
    }

    private Predicate<String> paths() {
        return regex("/.*");
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = ApiInfo.DEFAULT;
        return apiInfo;
    }
}