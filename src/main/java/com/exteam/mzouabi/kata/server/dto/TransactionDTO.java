package com.exteam.mzouabi.kata.server.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by MZO on 30/06/2016.
 */
public class TransactionDTO {

    Date transactionDate;

    BigDecimal amount;

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
