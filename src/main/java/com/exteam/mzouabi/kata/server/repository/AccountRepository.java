package com.exteam.mzouabi.kata.server.repository;

import com.exteam.mzouabi.kata.server.model.Account;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

}
