package com.exteam.mzouabi.kata.server.service;

import com.exteam.mzouabi.kata.server.dto.AccountDTO;
import com.exteam.mzouabi.kata.server.dto.TransactionDTO;
import com.exteam.mzouabi.kata.server.mapper.ApplicationMappers;
import com.exteam.mzouabi.kata.server.model.Account;
import com.exteam.mzouabi.kata.server.model.Transaction;
import com.exteam.mzouabi.kata.server.repository.AccountRepository;
import com.exteam.mzouabi.kata.server.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    final static Logger LOG = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Inject
    AccountRepository accountRepository;

    @Inject
    TransactionRepository transactionRepository;

    @Inject
    ApplicationMappers applicationMappers;

    @Override
    public Long createAccount(AccountDTO accountDTO) {
        Account account = new Account();
        applicationMappers.mapToAccountEntity(accountDTO, account);
        if (account.getBalance() == null) {
            account.setBalance(new BigDecimal(0));
        }
        account = accountRepository.save(account);

        LOG.info("Account {} created", account.getIban());
        return account.getId();
    }

    @Override
    public AccountDTO getAccount(Long accountId) {
        Account account = accountRepository.getOne(accountId);
        return applicationMappers.toAccountDTO(account);
    }

    @Override
    public void deposit(Long accoutnId, BigDecimal amount) {
        Account account = accountRepository.getOne(accoutnId);
        if (account == null) {
            throw new IllegalArgumentException("The account is unknown");
        }

        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("The amount must be greater than 0");
        }

        account.setBalance(account.getBalance().add(amount));
        Transaction transaction = buildTransaction(account, amount);

        transactionRepository.save(transaction);
        LOG.info("Account {} : dposit {}", account.getIban(), amount);
    }

    @Override
    public void withdraw(Long accountId, BigDecimal amount) {
        Account account = accountRepository.getOne(accountId);
        if (account == null) {
            throw new IllegalArgumentException("The account is unknown");
        }

        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("The amount must be greater than 0");
        }

        if (amount.compareTo(account.getBalance()) > 0) {
            throw new IllegalArgumentException("The amount is greater than the balance");
        }

        account.setBalance(account.getBalance().subtract(amount));

        Transaction transaction = buildTransaction(account, amount.negate());
        transactionRepository.save(transaction);
        LOG.info("Account {} : withdrawal {}", account.getIban(), amount);
    }

    /**
     * Méthode qui construit un objet transaction
     *
     * @param account
     * @param amount
     * @return
     */
    private Transaction buildTransaction(Account account, BigDecimal amount) {
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(amount);
        transaction.setTransactionDate(new Date());
        return transaction;
    }

    @Override
    public Page<TransactionDTO> getTransactions(Long accountId, Pageable pageable) {
        Account account = accountRepository.getOne(accountId);
        if (account == null) {
            throw new IllegalArgumentException("The account is unknown");
        }
        return transactionRepository.findByAccountId(accountId, pageable).map(transaction -> applicationMappers.toTransactioDTO(transaction));
    }
}
