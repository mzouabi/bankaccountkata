package com.exteam.mzouabi.kata.server.service;

import com.exteam.mzouabi.kata.server.dto.AccountDTO;
import com.exteam.mzouabi.kata.server.dto.TransactionDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/root-applicationContext.xml", "classpath:/data-applicationContext.xml", "classpath:/service-applicationContext.xml"})
public class AccountServiceTest {

    @Autowired
    AccountService accountService;

    Long accountId;

    @Before
    public void initAccount() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setIban("DE68210501700012345678");
        accountDTO.setBic("RAIFCH22");
        accountId = accountService.createAccount(accountDTO);
    }

    @Test
    public void test_account_transactions() {
        Assert.assertNotNull(accountId);

        // initial state
        AccountDTO accountDTO = accountService.getAccount(accountId);
        Assert.assertNotNull(accountDTO);
        Assert.assertEquals(0, new BigDecimal(0.0).compareTo(accountDTO.getBalance()));

        // first deposit
        accountService.deposit(accountId, new BigDecimal(100.50));
        accountDTO = accountService.getAccount(accountId);
        Assert.assertEquals(0, new BigDecimal(100.50).compareTo(accountDTO.getBalance()));

        // second deposit
        accountService.deposit(accountId, new BigDecimal(20));
        accountDTO = accountService.getAccount(accountId);
        Assert.assertEquals(0, new BigDecimal(120.50).compareTo(accountDTO.getBalance()));

        // first withdrawal
        accountService.withdraw(accountId, new BigDecimal(30));
        accountDTO = accountService.getAccount(accountId);
        Assert.assertEquals(0, new BigDecimal(90.50).compareTo(accountDTO.getBalance()));

        Page<TransactionDTO> transactions = accountService.getTransactions(accountId, new PageRequest(0, 100));

        Assert.assertEquals(3, transactions.getContent().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_validate_amount_greater_than_0() {
        Assert.assertNotNull(accountId);

        accountService.deposit(accountId, new BigDecimal(-100.50));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_validate_Withdrawal_amount_less_than_or_equals_balance() {
        Assert.assertNotNull(accountId);

        accountService.deposit(accountId, new BigDecimal(100.50));
        accountService.withdraw(accountId, new BigDecimal(100.51));
    }
}