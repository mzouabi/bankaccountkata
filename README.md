# Bank Account Kata

## Compiling

`mvn clean install`

## Running unit test

`mvn generate-sources` to generate mappers sources ( with mapstruct ) 
`mvn test`

## Running

`mvn jetty:run`

## Testing

Swagger Testing GUI is available on http://localhost:8080/accounts/swagger


